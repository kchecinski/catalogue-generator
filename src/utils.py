import json
import os

from config import OUT_DIR, PHOTOS_DIR, TRAITS_ICONS_DIR


def merge_documents(documents):
    pages = [p for doc in documents for p in doc.pages]
    pdf = documents[0].copy(pages).write_pdf()
    return pdf


def photo_path(photo_filename):
    return os.path.join(PHOTOS_DIR, photo_filename)


def trait_icon_path(trait_name, trait_value):
    return os.path.join(TRAITS_ICONS_DIR, F"{trait_name}_{trait_value}.png")


def list_into_chunks(elements_list, chunk_size):
    return [elements_list[i:i + chunk_size] for i in range(0, len(elements_list), chunk_size)]


def save_json(path: str, json_dict):
    with open(path, 'w') as file:
        json.dump(json_dict, file)


def load_json(path: str):
    with open(path, 'r') as file:
        return json.load(file)


def out_file_path(filename: str):
    return os.path.join(OUT_DIR, filename)


def json_filename(name: str):
    return F"{name}.json"
