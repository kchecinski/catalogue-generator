import itertools
from abc import ABC, abstractmethod
from typing import List

from html_content.html_content import HTMLContent
from templates import read_html_template, fill_template


class CataloguePart(HTMLContent, ABC):
    def __init__(self, start_page: int = None):
        super().__init__()
        self.start_page = start_page
        if self.start_page is not None:
            self.assign_pages(start_page)

    @abstractmethod
    def assign_pages(self, start_page):
        pass

    @abstractmethod
    def length(self):
        pass

    @property
    def begin(self):
        if self.start_page is None:
            raise ValueError("Page number is not set")
        return self.start_page


class PagesList(CataloguePart):
    def __init__(self, pages: List[CataloguePart], start_page: int = None):
        self.pages = pages
        super().__init__(start_page=start_page)

    def assign_pages(self, start_page):
        self.start_page = start_page
        current_page = start_page
        for page in self.pages:
            page.assign_pages(current_page)
            current_page += page.length()

    def length(self):
        return sum([page.length() for page in self.pages])

    def generate_html(self):
        result = []
        for part in self.pages:
            part_html = part.generate_html()
            if not isinstance(part_html, list):
                part_html = [part_html]
            result.append(part_html)
        return list(itertools.chain(*result))


class SinglePage(CataloguePart, ABC):
    def __init__(self, start_page: int = None):
        super().__init__(start_page=start_page)

    def assign_pages(self, start_page):
        self.start_page = start_page

    def length(self):
        return 1

    @staticmethod
    def generate_page_html(content_html: str, content_style: str) -> str:
        page_html_template = read_html_template('page')
        return fill_template(page_html_template,
                             content=content_html,
                             style=content_style)
