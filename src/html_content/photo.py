from html_content.image import Image
from utils import photo_path


class Photo(Image):
    def __init__(self, filename: str):
        path = photo_path(filename)
        super().__init__(path)