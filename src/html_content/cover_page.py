from html_content.page import SinglePage
from html_content.photo import Photo
from templates import fill_html_template, fill_css_template


class CoverPage(SinglePage):
    def __init__(self, title: str, subtitle: str, photo: Photo):
        super().__init__()
        self.title = title
        self.subtitle = subtitle
        self.photo = photo

    def generate_html(self):
        cover_page_html = fill_html_template('cover_page',
                                             title=self.title,
                                             subtitle=self.subtitle,
                                             image=self.photo.path)
        cover_page_css = fill_css_template('cover_page')
        return self.generate_page_html(content_html=cover_page_html,
                                       content_style=cover_page_css)
