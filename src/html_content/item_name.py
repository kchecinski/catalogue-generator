﻿from html_content.html_content import HTMLContent


# class ItemName(HTMLContent):
#     def __init__(self, name, customary_name=None):
#         super().__init__()
#         self.name = name
#         self.customary_name = customary_name
#
#     def generate_html(self, **kwargs):
#         html = F'<span class="item_name">{self.name}</span>'
#         if self.customary_name is not None and self.customary_name != '':
#             html += F'<br><span class="customary_name">({self.customary_name})</span>'
#         return html

CURRENCY_SYMBOL = "zł"


class ItemName(HTMLContent):
    def __init__(self, name, price=None):
        super().__init__()
        self.name = name
        self.price = price

    def price_html(self):
        if self.price is not None:
            return F'<span class="item_price"> - {self.price:.2f} {CURRENCY_SYMBOL}</span>'
        return ""

    def name_html(self):
        return F'<span class="item_name">{self.name}</span>'

    def generate_html(self, **kwargs):
        return self.name_html() + " " + self.price_html()
