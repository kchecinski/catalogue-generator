from typing import Union, List

from html_content.page import PagesList
from html_content.photo import Photo
from html_content.section_cover import SectionCover
from html_content.subsection import Subsection
from color_set import ColorSet


class Section(PagesList):
    def __init__(self, title: str, cover_photos: Union[Photo, List[Photo]], color_set: ColorSet,
                 content: List[Subsection], start_page: int = None):
        self.cover = SectionCover(title=title, photos=cover_photos)
        pages = [self.cover] + content
        super().__init__(pages, start_page)
        self.title = title
        self.color_set = color_set
        self.content = content
        self.set_content_color()

    def set_content_color(self):
        self.cover.color_set = self.color_set['section_cover']
        for elem in self.content:
            if isinstance(elem, Subsection):
                elem.color_set = self.color_set['subsection_page']
            else:
                raise ValueError('Undefined type')
