from html_content.html_content import HTMLContent
from html_content.photo import Photo
from templates import fill_html_template


class SectionsIndexElement(HTMLContent):
    def __init__(self, section_title: str, photo: Photo, page: int):
        self.section_title = section_title
        self.photo = photo
        self.section_page = page
        super().__init__()

    def generate_html(self):
        return fill_html_template('sections_index_element',
                                  item_path=self.photo.path,
                                  section_name=self.section_title,
                                  section_page=self.section_page)
