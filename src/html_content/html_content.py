from abc import ABC, abstractmethod


class HTMLContent(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def generate_html(self, **kwargs):
        pass
