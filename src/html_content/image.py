import os

from config import PHOTOS_DIR
from get_image_size import get_image_size
from html_content.html_content import HTMLContent
from templates import fill_html_template


def build_image_path(image_name):
    return os.path.join(PHOTOS_DIR, image_name)


class Image(HTMLContent):
    def __init__(self, path: str):
        super().__init__()
        self.path = path

    def get_size(self):
        return get_image_size(self.path)

    def generate_html(self):
        return fill_html_template("image",
                                  image_path=self.path)

    def is_horizontal(self):
        size = self.get_size()
        return size[0] > size[1]
