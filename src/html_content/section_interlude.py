from typing import List

from html_content.page import SinglePage
from html_content.photo import Photo


class SectionInterlude(SinglePage):
    def __init__(self, photos=List[Photo], text: str = None, start_page: int = None, color_set=None):
        super().__init__(start_page=start_page)
        self.photos = photos
        self.text = text
        self.color_set = color_set
        if self.color_set is None:
            self.color_set = dict()

    def generate_html(self):
        return []  # TODO
