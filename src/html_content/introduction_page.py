from html_content.default_page import DefaultPage
from html_content.legend import Legend
from templates import fill_html_template, fill_css_template


class IntroductionPage(DefaultPage):
    def __init__(self, introduction_text: str, legend: Legend, color_set):
        super().__init__(color_set)
        self.introduction_text = introduction_text
        self.legend = legend

    def _content_html(self):
        legend_html = self.legend.generate_html()
        return fill_html_template('introduction_page',
                                  introduction_text=self.introduction_text,
                                  legend_html=legend_html)

    def _content_style(self):
        legend_style = self.legend.generate_style()
        return fill_css_template('introduction_page',
                                 legend_style=legend_style)

    def _title_html(self):
        return "Wprowadzenie"
