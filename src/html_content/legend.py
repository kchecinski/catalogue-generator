from typing import List

import pandas as pd

from html_content.html_content import HTMLContent
from html_content.traits_panel import TraitIcon
from templates import fill_html_template, fill_css_template


class Legend(HTMLContent):
    def __init__(self, legend_dataframe: pd.DataFrame):
        self.rows = legend_dataframe_to_rows(legend_dataframe)
        super().__init__()

    def generate_html(self, **kwargs):
        rows_html = "\n".join([row.generate_html() for row in self.rows])
        return fill_html_template("legend",
                                  legend_rows=rows_html)

    def generate_style(self):
        return fill_css_template("legend")


class LegendRow(HTMLContent):
    def __init__(self, trait_name: str, icons: List[TraitIcon], names: List[str]):
        super().__init__()
        self.trait_name = trait_name
        if self.trait_name is None:
            self.trait_name = ""
        self.elements = [LegendElement(name, icon) for name, icon in zip(names, icons)]

    def generate_html(self, **kwargs):
        elements_html = "\n".join([elem.generate_html() for elem in self.elements])
        return fill_html_template("legend_row",
                                  trait_name=self.trait_name,
                                  legend_elements=elements_html)


class LegendElement(HTMLContent):
    def __init__(self, name: str, icon: TraitIcon):
        super().__init__()
        self.name = name
        self.icon = icon

    def generate_html(self, **kwargs):
        return fill_html_template('legend_element',
                                  icon=self.icon.generate_html(),
                                  caption=self.name)


def legend_dataframe_to_rows(legend_dataframe: pd.DataFrame) -> List[LegendRow]:
    rows = []
    values = legend_dataframe.columns.values[2:]
    for index, df_row in legend_dataframe.iterrows():
        trait_text = df_row['name_text']
        trait_name = df_row['name']
        values_texts = df_row[values].dropna()
        icons = [TraitIcon(trait_name, value) for value in values_texts.index.values]
        texts = values_texts.values
        rows.append(LegendRow(trait_text, icons, texts))
    return rows
