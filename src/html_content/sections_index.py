from typing import List

from html_content.page import PagesList
from html_content.section import Section
from html_content.sections_index_page import SectionsIndexPage
from templates import html_template_placeholders
from utils import list_into_chunks


class SectionsIndex(PagesList):
    def __init__(self, n_sections: int, color_set):
        self.color_set = color_set
        self.n_page_slots = len(html_template_placeholders('sections_index_layout'))
        pages = self._generate_pages(n_sections)
        super().__init__(pages)

    def _generate_pages(self, n_sections) -> List[SectionsIndexPage]:
        n_pages = n_sections // self.n_page_slots + 1
        return [SectionsIndexPage(color_set=self.color_set) for _ in range(n_pages)]

    def set_sections(self, sections: List[Section]):
        chunks = list_into_chunks(sections, self.n_page_slots)
        if len(chunks) != len(self.pages):
            raise ValueError('Number of sections chunks and number of the index\'s pages are not even')
        for chunk, page in zip(chunks, self.pages):
            if not isinstance(page, SectionsIndexPage):
                raise TypeError('Page should be instance od SectionsIndexPage')
            page.set_sections(chunk)
