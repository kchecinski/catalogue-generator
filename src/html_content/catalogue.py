from typing import List

from color_set import build_color_set
from html_content.cover_page import CoverPage
from html_content.introduction_page import IntroductionPage
from html_content.legend import Legend
from html_content.photo import Photo
from html_content.page import PagesList
from html_content.section import Section
from html_content.sections_index import SectionsIndex


class Catalogue(PagesList):
    def __init__(self,
                 title: str,
                 subtitle: str,
                 cover_photo: Photo,
                 introduction_text: str,
                 legend: Legend,
                 sections: List[Section]):
        self.cover_page = CoverPage(title, subtitle, photo=cover_photo)
        self.sections_index = SectionsIndex(n_sections=len(sections),
                                            color_set=build_color_set('test.json'))
        self.introduction_page = IntroductionPage(introduction_text, legend,
                                                  color_set=build_color_set('test.json'))
        # TODO Replace color sets with values from config
        pages = [self.cover_page, self.sections_index, self.introduction_page] + sections
        super().__init__(start_page=1, pages=pages)
        self.sections_index.set_sections(sections)
        self.title = title
        self.subtitle = subtitle
        self.sections = sections
