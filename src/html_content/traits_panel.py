from typing import Dict

from html_content.html_content import HTMLContent
from html_content.image import Image
from utils import trait_icon_path


binary_traits = ['winter_in_house']


def is_trait_binary(name: str) -> bool:
    return name in binary_traits


def build_trait_icon(name, value):
    return TraitIcon(name, value, is_trait_binary(name))


class TraitIcon(Image):
    def __init__(self, name: str, value: str, binary: bool = False):
        super().__init__(path=trait_icon_path(name, value))
        self.name = name
        self.value = value
        self.binary = binary

    def generate_html(self):
        if self.is_visible():
            return F"""
            <div class="trait_icon">{super().generate_html()}</div>
            """
        else:
            return ""

    def is_visible(self):
        return not self.binary or int(self.value) != 0


class TraitsIconsPanel(HTMLContent):
    def __init__(self, traits_values_dict: Dict[str, str]):
        self.traits = [build_trait_icon(name, value) for name, value in traits_values_dict.items()]
        super().__init__()

    def generate_html(self, **kwargs):
        return '\n'.join([trait.generate_html() for trait in self.traits])
