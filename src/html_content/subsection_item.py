from html_content.html_content import HTMLContent
from html_content.item_name import ItemName
from html_content.photo import Photo
from html_content.traits_panel import TraitsIconsPanel
from item import Item
from pages_layouts import fill_subsection_item_html_template


class SubsectionItem(HTMLContent):
    def __init__(self, item: Item, layout_name: str):
        self.item = item
        self.layout_name = layout_name
        self.photo = Photo(item.photo_path)
        # self.name = ItemName(self.item.name, self.item.customary_name)
        self.name = ItemName(self.item.name, self.item.price)
        self.traits_panel = TraitsIconsPanel({"insolation": item.insolation,
                                              "watering": item.watering,
                                              "winter_in_house": item.winter_in_house})
        super().__init__()

    def generate_html(self, **kwargs):
        name_html = self.name.generate_html()
        return fill_subsection_item_html_template(self.layout_name,
                                                  item_id=self.item.item_id,
                                                  item_image=self.photo.path,
                                                  item_name=name_html,
                                                  traits_icons=self.traits_panel.generate_html())
