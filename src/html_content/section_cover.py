from typing import Union, List

from html_content.page import SinglePage
from html_content.photo import Photo
from templates import fill_html_template, fill_css_template


class SectionCover(SinglePage):
    def __init__(self, title: str, photos: Union[Photo, List[Photo]], color_set=None):
        super().__init__()
        self.title = title
        self.photos = photos
        self.color_set = color_set
        if self.color_set is None:
            self.color_set = dict()

    def generate_html(self):
        section_cover_html = fill_html_template('section_cover',
                                                title=self.title,
                                                photo1=self.photos[0].path,
                                                photo2=self.photos[1].path)
        section_cover_css = fill_css_template('section_cover', **self.color_set)
        return self.generate_page_html(content_html=section_cover_html,
                                       content_style=section_cover_css)
