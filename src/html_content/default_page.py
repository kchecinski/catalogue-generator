from abc import ABC, abstractmethod
from datetime import date

from html_content.page import SinglePage
from templates import fill_html_template, fill_css_template


class DefaultPage(SinglePage, ABC):
    def __init__(self, color_set):
        self.color_set = color_set
        super().__init__()

    def generate_html(self, **kwargs):
        content_html = self._content_html()
        title_html = self._title_html()
        page_html = fill_html_template('default_page',
                                       title=title_html,
                                       page_content=content_html,
                                       page_number=self.start_page,
                                       update_date=date.today().strftime("%d-%m-%Y"))
        content_css = self._content_style()
        page_style = fill_css_template('default_page',
                                       page_content_style=content_css,
                                       **self.color_set['default_page'])
        return self.generate_page_html(content_html=page_html,
                                       content_style=page_style)

    @abstractmethod
    def _content_html(self):
        pass

    @abstractmethod
    def _content_style(self):
        pass

    @abstractmethod
    def _title_html(self):
        pass
