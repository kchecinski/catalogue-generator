from datetime import date

import config
from html_content.page import SinglePage
from html_content.photo import Photo
from templates import fill_template_from_file, fill_html_template, fill_css_template


class SectionItem(SinglePage):
    def __init__(self,
                 name: str,
                 customary_name: str,
                 price: float,
                 photo: Photo,
                 section_name: str,
                 description: str = "",
                 color_set=None):
        super().__init__()
        self.name = name
        self.customary_name = customary_name
        self.price = price
        self.photo = photo
        self.section_name = section_name
        self.description = description
        self.color_set = color_set
        if self.color_set is None:
            self.color_set = dict()

    def generate_html(self):
        layout_name = self.determine_layout()
        template_path = config.ITEM_PAGE_TEMPLATE_DIR
        customary_name = "" if self.customary_name == "" else F"({self.customary_name})"
        layout_html = fill_html_template(layout_name,
                                         section=self.section_name,
                                         description=self.description,
                                         name=self.name,
                                         customary_name=customary_name,
                                         price=self.price,
                                         item_image=self.photo.path,
                                         page=self.begin,
                                         update_date=date.today().strftime("%d-%m-%Y")
                                         )
        css = fill_css_template(layout_name, **self.color_set)
        template = fill_template_from_file(template_path,
                                           style=css,
                                           content=layout_html)
        return template

    def determine_layout(self):
        is_image_horizontal = self.photo.is_horizontal()
        is_description_empty = self.description == ""
        if is_description_empty:
            if is_image_horizontal:
                return "item_page_hphoto_nodesc_layout"
            else:
                return "item_page_vphoto_nodesc_layout"
        else:
            if is_image_horizontal:
                return "item_page_hphoto_layout"
            else:
                return "item_page_vphoto_layout"
