from typing import List

from html_content.page import PagesList
from html_content.subsection_page import SubsectionPage
from item import Item
from subsection_page_layout import subsection_layout_slots_by_name
from utils import list_into_chunks


class Subsection(PagesList):
    def __init__(self, items: List[Item], section_name: str, name: str, layout_name: str, page_number: int = None,
                 color_set=None):
        self.items = items
        self.section_name = section_name
        self.name = name
        self.layout_name = layout_name
        self.page_number = page_number
        self.color_set = color_set
        pages = self._generate_pages()
        super().__init__(pages)

    def _generate_pages(self):
        layout_slots = subsection_layout_slots_by_name(self.layout_name)
        items_chunks = list_into_chunks(self.items, len(layout_slots))
        pages = [SubsectionPage(items=chunk, layout_name=self.layout_name, section_name=self.section_name,
                                subsection_name=self.name, color_set=self.color_set) for i, chunk in enumerate(items_chunks)]
        return pages
