from typing import List

from html_content.default_page import DefaultPage
from html_content.subsection_item import SubsectionItem
from item import Item
from pages_layouts import fill_subsection_page_layout_html_template, fill_subsection_page_layout_css_template
from subsection_page_layout import subsection_layout_slots_by_name
from templates import fill_html_template, fill_css_template


class SubsectionPage(DefaultPage):
    def __init__(self, items: List[Item], layout_name: str, section_name,
                 subsection_name=None, color_set=None):
        self.items = items
        self.layout_name = layout_name
        self.section_name = section_name
        self.subsection_name = subsection_name
        if len(items) > self.capacity():
            raise ValueError("Number of items exceeding layout capacity")
        self.items = [SubsectionItem(item, layout_name=layout_name) for item in items]
        super().__init__(color_set)

    def capacity(self):
        return len(self.layout_slots())

    def layout_slots(self):
        return subsection_layout_slots_by_name(self.layout_name)

    def _content_html(self):
        items_html = self._generate_items_html()
        layout_html = fill_subsection_page_layout_html_template(self.layout_name, **items_html)
        return layout_html

    def _content_style(self):
        layout_css = fill_subsection_page_layout_css_template(self.layout_name,
                                                              **self.color_set['subsection_layout'])
        subsection_page_css = fill_css_template('subsection_page',
                                                **self.color_set['subsection_page'],
                                                layout_style=layout_css)
        return subsection_page_css

    def _title_html(self):
        return fill_html_template('subsection_page_title',
                                  section=self.section_name,
                                  subsection=self.subsection_title_text())

    def _generate_items_html(self):
        slots = self.layout_slots()
        items_html = {slot: item.generate_html() for slot, item in zip(slots, self.items)}
        missing_slots = list(set(slots) - set(items_html.keys()))
        for slot in missing_slots:
            items_html[slot] = ""
        return items_html

    def subsection_title_text(self):
        return F"{self.subsection_name}" if self.subsection_name is not None else ""
