from typing import List

from html_content.default_page import DefaultPage
from html_content.section import Section
from html_content.sections_index_element import SectionsIndexElement
from templates import fill_html_template, html_template_placeholders, fill_css_template


class SectionsIndexPage(DefaultPage):
    def _content_html(self):
        layout_fields = html_template_placeholders('sections_index_layout')
        elements_html = [element.generate_html() for element in self.elements]
        n_missing_elements = self.capacity() - len(self.elements)
        elements_html += [''] * n_missing_elements
        layout_elements = {field: element_html for field, element_html in zip(layout_fields, elements_html)}
        return fill_html_template('sections_index_layout',
                                  **layout_elements)

    def _content_style(self):
        element_style = fill_css_template('sections_index_element',
                                          **self.color_set['sections_index_element'])
        layout_style = fill_css_template('sections_index_layout')
        page_style = fill_css_template('sections_index_page',
                                       **self.color_set['sections_index_page'],
                                       element_style=element_style,
                                       layout_style=layout_style)
        return page_style

    def _title_html(self):
        return "Spis sekcji"

    def __init__(self, color_set):
        self.elements = []
        super().__init__(color_set)

    # def generate_html(self, **kwargs):
    #     layout_fields = html_template_placeholders('sections_index_layout')
    #     elements_html = [element.generate_html() for element in self.elements]
    #     n_missing_elements = self.capacity() - len(self.elements)
    #     elements_html += [''] * n_missing_elements
    #     layout_elements = {field: element_html for field, element_html in zip(layout_fields, elements_html)}
    #     layout_html = fill_html_template('sections_index_layout',
    #                                      **layout_elements)
    #     content_html = fill_html_template('sections_index_page',
    #                                       layout=layout_html,
    #                                       page=self.start_page,
    #                                       update_date=date.today().strftime("%d-%m-%Y"))
    #     element_style = fill_css_template('sections_index_element',
    #                                       **self.color_set['sections_index_element'])
    #     layout_style = fill_css_template('sections_index_layout')
    #     page_style = fill_css_template('sections_index_page',
    #                                    **self.color_set['sections_index_page'],
    #                                    element_style=element_style,
    #                                    layout_style=layout_style)
    #     return self.generate_page_html(content_html=content_html,
    #                                    content_style=page_style)

    def capacity(self):
        return len(html_template_placeholders('sections_index_layout'))

    def set_sections(self, sections: List[Section]):
        if len(sections) > self.capacity():
            raise ValueError("Number of given sections exceeds number of slots in the page layout")
        self.elements = [SectionsIndexElement(section.title,
                                              section.cover.photos[0],
                                              section.start_page) for section in sections]
