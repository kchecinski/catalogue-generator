from templates import read_html_template, read_css_template, fill_html_template, fill_css_template


def subsection_page_template_name(layout_name: str) -> str:
    return F"{layout_name}_subsection_page_layout"


def subsection_item_template_name(layout_name: str) -> str:
    return F"{layout_name}_subsection_item"


def read_subsection_page_layout_css_template(layout_name: str):
    template_name = F"{layout_name}_subsection_page_layout"
    return read_css_template(template_name)


def read_subsection_page_layout_html_template(layout_name: str):
    template_name = F"{layout_name}_subsection_page_layout"
    return read_html_template(template_name)


def read_subsection_item_html_template(layout_name: str):
    template_name = F"{layout_name}_subsection_item"
    return read_html_template(template_name)


def fill_subsection_page_layout_html_template(layout_name: str, **kwargs):
    template_name = subsection_page_template_name(layout_name)
    return fill_html_template(template_name, **kwargs)


def fill_subsection_page_layout_css_template(layout_name: str, **kwargs):
    template_name = subsection_page_template_name(layout_name)
    return fill_css_template(template_name, **kwargs)


def fill_subsection_item_html_template(layout_name: str, **kwargs):
    template_name = subsection_item_template_name(layout_name)
    return fill_html_template(template_name, **kwargs)
