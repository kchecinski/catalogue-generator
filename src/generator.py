import os
from typing import Dict, Any, Union, List

import pandas as pd
import weasyprint

from color_set import build_color_set
from config import OUT_DIR
from html_content.catalogue import Catalogue
from html_content.legend import Legend
from html_content.photo import Photo
from html_content.section import Section
from html_content.section_interlude import SectionInterlude
from html_content.subsection import Subsection
from item import Item
from utils import merge_documents, load_json


def empty_columns(df: pd.DataFrame):
    return [col for col in df.columns if (df[col] == '').all()]


def remove_empty_columns(df: pd.DataFrame):
    return df.drop(columns=empty_columns(df))


def read_excel_content(path):
    content = pd.read_excel(path, engine='openpyxl',
                            dtype={'insolation': str, 'watering': str}).fillna('')
    content = content[content['id'] != '']
    content['id'] = content['id'].astype(int)
    content = remove_empty_columns(content)
    return content


def read_csv_content(path):
    return pd.read_csv(path)


def read_content(path, ext='excel'):
    if ext == 'excel':
        content = read_excel_content(path)
    elif ext == 'csv':
        content = read_csv_content(path)
    else:
        raise ValueError(F"Unknown extension: {ext}")
    content = content.set_index('id')
    content['watering'] = content['watering']
    content['insolation'] = content['insolation']
    return content


def load_structure(name):
    filename = F"{name}.json"
    path = os.path.join(OUT_DIR, filename)
    return load_json(path)


def build_catalogue(structure: Dict[str, Any], content: pd.DataFrame) -> Catalogue:
    title = structure['title']
    subtitle = structure['subtitle']
    cover_photo = Photo(structure['cover_photo'])
    sections_structure = structure['sections']
    sections = [build_section(sec_struct, content) for sec_struct in sections_structure]
    legend = Legend(pd.read_json(structure['legend']))
    introduction_text = structure['introduction']
    return Catalogue(title=title,
                     subtitle=subtitle,
                     cover_photo=cover_photo,
                     legend=legend,
                     introduction_text=introduction_text,
                     sections=sections)


def build_section(structure: Dict[str, Any], content: pd.DataFrame):
    title = structure['title']
    cover_photos = [Photo(p) for p in structure['cover']]
    color_set = build_color_set(structure['color_set'])
    layout_name = structure.get('layout', None)
    content = [build_subsection(subsection,
                                content=content,
                                section_name=title,
                                color_set=color_set,
                                section_layout_name=layout_name) for subsection in structure['content']]
    return Section(title=title, cover_photos=cover_photos, color_set=color_set, content=content)


def build_subsection(structure: Dict[str, Any],
                     content: pd.DataFrame,
                     section_name: str,
                     color_set,
                     section_layout_name: str = None) -> Subsection:
    subsection_name = structure.get("name", None)
    items_ids = structure['content']
    subsection_layout_name = structure.get("layout", None)
    subsection_layout_name = subsection_layout_name if subsection_layout_name is not None else section_layout_name
    items = [section_item_by_row(content.loc[i]) for i in items_ids]
    return Subsection(items=items, section_name=section_name, name=subsection_name, layout_name=subsection_layout_name,
                      color_set=color_set)


def section_item_by_row(item_row):
    return Item(item_id=int(item_row.name),
                name=item_row['name'],
                price=item_row['price'], photo_path=item_row['photo'],
                insolation=item_row.get('insolation', None),
                watering=item_row.get('watering', None),
                winter_in_house=item_row.get('winter_in_house', None),
                customary_name=item_row['customary_name'])


def build_section_element(structure: Union[int, Dict[str, Any]], content: pd.DataFrame):
    if isinstance(structure, int):
        item_row = content.loc[structure]
        return section_item_by_row(item_row)
    elif isinstance(structure, dict):
        if "id" in structure.keys():
            item_row = content.loc[structure['id']]
            return section_item_by_row(item_row)
        elif "photos" in structure.keys():
            return SectionInterlude([Photo(p) for p in structure['photos']],
                                    text=structure.get('text', None))


def generate_pdf(htmls: Union[List[str], str], out: str):
    if isinstance(htmls, str):
        htmls = [htmls]
    documents = [weasyprint.HTML(string=html, base_url='.', encoding='utf-8').render() for html in htmls]
    pdf = merge_documents(documents)
    with open(out, 'wb') as file:
        file.write(pdf)
