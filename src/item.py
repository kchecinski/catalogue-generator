class Item:
    def __init__(self,
                 item_id: int,
                 name: str,
                 price: float,
                 photo_path: str,
                 insolation: str,
                 watering: str,
                 winter_in_house: bool,
                 customary_name: str = None):
        self.item_id = item_id
        self.name = name
        self.price = price
        self.customary_name = customary_name
        self.photo_path = photo_path
        self.insolation = insolation
        self.watering = watering
        self.winter_in_house = winter_in_house
