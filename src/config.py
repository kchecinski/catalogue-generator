import os

TEMPLATES_DIR = "templates"

HTML_TEMPLATES_DIR = os.path.join(TEMPLATES_DIR, "html")
CSS_TEMPLATES_DIR = os.path.join(TEMPLATES_DIR, "css")

TEMPLATES_HTML_DIR = os.path.join(TEMPLATES_DIR, "html")
TEMPLATES_CSS_DIR = os.path.join(TEMPLATES_DIR, "css")

ASSETS_DIR = "assets"

PHOTOS_DIR = os.path.join(ASSETS_DIR, "photos")
TRAITS_ICONS_DIR = os.path.join(ASSETS_DIR, "traits")

COLOR_SETS_DIR = "color_sets"

ITEM_PAGE_TEMPLATE_DIR = os.path.join(TEMPLATES_HTML_DIR, "page.html")

SUBSECTION_LAYOUTS_DIR_DICT = {
    "2x2grid": os.path.join(TEMPLATES_HTML_DIR, "2x2grid_subsection_page_layout.html")
}

SUBSECTION_CSS_DIR_DICT = {
    "2x2grid": os.path.join(TEMPLATES_CSS_DIR, "2x2grid_subsection_page_layout.css")
}


OUT_DIR = "out"
