import json
import os
from typing import Any, Dict

import pandas as pd

from config import OUT_DIR
from generator import read_excel_content
from utils import json_filename


def read_excel_sheet(path: str, sheet_name: str):
    return pd.read_excel(path, engine='openpyxl', sheet_name=sheet_name)


def cohesive_groups_values(df: pd.DataFrame, column_name):
    return (df[column_name] != df[column_name].shift(1)).cumsum() - 1


def generate_subsections_from_content(section_content: pd.DataFrame):
    section_content['subsection_idx'] = cohesive_groups_values(section_content, column_name='subsection')
    subsections_dicts = []
    for subsection_idx, subsection_df in section_content.groupby('subsection_idx'):
        subsection_dict = {}
        if subsection_df['subsection'].iloc[0] != '':
            subsection_dict['name'] = subsection_df['subsection'].iloc[0]
        subsection_dict['content'] = list(subsection_df['id'])
        subsections_dicts.append(subsection_dict)
    return subsections_dicts


def generate_structure(content: pd.DataFrame, sections_config: pd.DataFrame):
    sections_config = sections_config.set_index('title')
    sections_dicts = []
    for section_name, section_df in content.groupby('type'):
        section_config = sections_config.loc[section_name]
        section_dict = {
            'title': section_name,
            'cover': [section_config['cover_photo_A'],
                      section_config['cover_photo_B']],
            'color_set': section_config['color_set'],
            'layout': section_config['layout'],
            'content': generate_subsections_from_content(section_df)
        }
        sections_dicts.append(section_dict)

    return sections_dicts


def read_sections_config(path: str):
    return read_excel_sheet(path, 'sections_config')


def read_general_config(path: str):
    df = read_excel_sheet(path, 'general')
    return pd.Series(df['value'].values, index=df['attribute'])


def save_structure(structure: Dict[str, Any], name: str):
    filename = json_filename(name)
    path = os.path.join(OUT_DIR, filename)
    with open(path, 'w+') as file:
        json.dump(structure, file)


def read_legend_config(path: str):
    df = read_excel_sheet(path, 'legend')
    return df.to_json()


def generate_catalogue_structure_from_xlsx(content_path: str,
                                           structure_path: str):
    content = read_excel_content(content_path)
    content = content[content['available'].astype(bool)]
    sections_config = read_sections_config(content_path)
    general_config = read_general_config(content_path)
    sections_dict = generate_structure(content, sections_config)
    legend_dict = read_legend_config(content_path)
    structure = {
        "title": general_config['title'],
        "subtitle": general_config['subtitle'],
        "introduction": general_config['introduction'],
        "cover_photo": general_config['cover_photo'],
        "legend": legend_dict,
        "sections": sections_dict,
    }
    save_structure(structure, structure_path)
