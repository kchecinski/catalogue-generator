import os
from string import Template
from typing import List

from config import HTML_TEMPLATES_DIR, CSS_TEMPLATES_DIR


def read_html_template(template_name: str) -> Template:
    template_filename = F"{template_name}.html"
    template_path = os.path.join(HTML_TEMPLATES_DIR, template_filename)
    with open(template_path, 'r') as file:
        return Template(file.read())


def read_css_template(template_name: str) -> Template:
    template_filename = F"{template_name}.css"
    template_path = os.path.join(CSS_TEMPLATES_DIR, template_filename)
    with open(template_path, 'r') as file:
        return Template(file.read())


def read_template(path: str):
    with open(path, 'r') as file:
        return Template(file.read())


def fill_html_template(template_name: str, **kwargs) -> str:
    template = read_html_template(template_name)
    return template.substitute(**kwargs)


def fill_css_template(template_name: str, **kwargs) -> str:
    template = read_css_template(template_name)
    return template.substitute(**kwargs)


def html_template_placeholders(template_name: str) -> List[str]:
    template = read_html_template(template_name)
    return template_placeholders(template)


def css_template_placeholders(template_name: str) -> List[str]:
    template = read_css_template(template_name)
    return template_placeholders(template)


def template_placeholders(template):
    return [m.group('named') or m.group('braced')
            for m in template.pattern.finditer(template.template)
            if m.group('named') or m.group('braced')]


def fill_template(template, **kwargs):
    filled_template = template.substitute(**kwargs)
    return filled_template


def fill_template_from_file(path, **kwargs):
    template = read_template(path)
    return fill_template(template, **kwargs)
