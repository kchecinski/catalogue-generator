from generator import build_catalogue, read_content, load_structure, generate_pdf
from generate_structure import generate_catalogue_structure_from_xlsx

import argparse


def generate_catalogue(content_filename,
                       structure_filename,
                       out_filename):
    generate_catalogue_structure_from_xlsx(content_filename, structure_filename)

    content = read_content(content_filename)
    structure = load_structure(structure_filename)
    catalogue = build_catalogue(structure, content)

    html = catalogue.generate_html()
    generate_pdf(html, out=out_filename)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--content', type=str, default='content.xlsx', help='content xlsx filename')
    parser.add_argument('--structure', type=str, default='structure', help='structure filename')
    parser.add_argument('--out', type=str, default='out.pdf', help='out filename')
    args = parser.parse_args()
    generate_catalogue(content_filename=args.content,
                       structure_filename=args.structure,
                       out_filename=args.out)
