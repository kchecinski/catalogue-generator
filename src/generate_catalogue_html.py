from generator import build_catalogue, read_content, load_structure, generate_pdf
from generate_structure import generate_catalogue_structure_from_xlsx

import os
import argparse


def save_pages_html(pages_html, out_dir):
    if not os.path.isdir(out_dir):
        if os.path.isfile(out_dir):
            raise FileExistsError(F"File {out_dir} exists")
        os.mkdir(out_dir)
    else:
        dir_content_paths = [os.path.join(out_dir, filename) for filename in os.listdir(out_dir)]
        for path in dir_content_paths:
            os.remove(path)
    for i, page in enumerate(pages_html):
        file_path = os.path.join(out_dir, F"{i}.html")
        with open(file_path, 'w+', encoding='utf-8') as file:
            file.write(page)


def generate_catalogue_html(content_filename,
                            structure_filename,
                            out_dir):
    generate_catalogue_structure_from_xlsx(content_filename, structure_filename)

    content = read_content(content_filename)
    structure = load_structure(structure_filename)
    catalogue = build_catalogue(structure, content)

    html = catalogue.generate_html()
    save_pages_html(pages_html=html, out_dir=out_dir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--content', type=str, default='content.xlsx', help='content xlsx filename')
    parser.add_argument('--structure', type=str, default='structure', help='structure filename')
    parser.add_argument('--out', type=str, default='out_html', help='out filename')
    args = parser.parse_args()
    generate_catalogue_html(content_filename=args.content,
                            structure_filename=args.structure,
                            out_dir=args.out)
