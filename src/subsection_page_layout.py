import re

from config import SUBSECTION_LAYOUTS_DIR_DICT, SUBSECTION_CSS_DIR_DICT


def layout_slots(layout_template: str, key='item'):
    return re.findall(F'{key}[0-9]*', layout_template)


def subsection_layout_slots_by_name(layout_name: str, key='item'):
    layout = read_subsection_layout_html(layout_name)
    return layout_slots(layout, key=key)


def read_subsection_layout_html(layout_name: str):
    with open(SUBSECTION_LAYOUTS_DIR_DICT[layout_name], 'r') as file:
        return file.read()


def read_subsection_layout_css(layout_name: str):
    with open(SUBSECTION_CSS_DIR_DICT[layout_name], 'r') as file:
        return file.read()