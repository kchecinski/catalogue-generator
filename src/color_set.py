import json
import os
from typing import Union, Dict

from config import COLOR_SETS_DIR


def build_color_set(colors_dict: Union[str, Dict[str, str]]):
    if isinstance(colors_dict, str):
        return ColorSet.read(colors_dict)
    else:
        return ColorSet(**colors_dict)


class ColorSet:
    def __init__(self, colors, mapping):
        self.colors = colors
        self.mapping = mapping
        self.mapped_colors = {}
        for filetype, filetype_colors in self.mapping.items():
            self.mapped_colors[filetype] = {}
            for element, color_name in filetype_colors.items():
                self.mapped_colors[filetype][element] = self.colors[color_name]

    @staticmethod
    def read(filename):
        colors_filepath = os.path.join(COLOR_SETS_DIR, filename)
        mapping_filepath = os.path.join(COLOR_SETS_DIR, 'colors_mapping.json')
        with open(colors_filepath, 'r') as json_file:
            colors_dict = json.load(json_file)
        with open(mapping_filepath, 'r') as json_file:
            mapping = json.load(json_file)
        return ColorSet(colors_dict, mapping)

    def __getitem__(self, item):
        return self.mapped_colors[item]
